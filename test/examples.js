/* eslint-disable no-trailing-spaces */
/* eslint no-undef:0 */


describe('EXEMPLOS DE ECMAScrip-6', function() {
  it('ITERAÇÕES COM ARRAY - FOREACH', () => {
    /*
         O forEach é o  método que  passar por todos os elementos de  dentro de um Array. Considere
     o caso no qual temos de mostrar no console todos os itens de uma lista de nomes.

     * Metodo tradicional:
    */
    var nomes = ['maria', 'josé', 'joão']
   for(var i = 0; i < nomes.length; i++) {
      console.log(nomes[i])
    }
    /*
    Resultado
    +--------------------------------------------+
    |   maria, josé, joão                        |
    +--------------------------------------------+

     * Utilizando a função forEach no Array , e passamos como parâmetro uma função de retorno que aceita um outro
     parâmetro. Neste exemplo, usaremos a função anônima function(nome){...} : */

     var nomes1 = ['maria', 'josé', 'joão']
     nomes1.forEach(function(nome) {
     console.log(nome)
     })
    /*
     Resultado
     +--------------------------------------------+
     |   maria, josé, joão                        |
     +--------------------------------------------+
     Passamos para o  forEach uma função anônima de retorno, que costumamos chamar de função de callback.
     Ela é executada para cada elemento dentro da lista. A cada iteração, o valor da lista é atribuído à variável
     passada como parâmetro no callback — no nosso caso, a variável nome. Neste exemplo, somente emitimos no console
     seu valor. Mas nesta função de callback , podemos fazer qualquer coisa com o valor da variável, inclusive
     passá-la como parâmetro para outros métodos.

     Entretanto, note que a função de callback não precisa necessariamente ser anônima. Podemos defini-la antes e
     atribuí-la a uma variável para passá-la como parâmetro ao forEach : */

     var nomes2 = ['maria', 'josé', 'joão'];
     function imprimeNome(nome) {
        console.log(nome);
     }
     nomes2.forEach(imprimeNome);
     /*
     Resultado
        +--------------------------------------------+
        |   maria, josé, joão                        |
        +--------------------------------------------+
     Bem legal, não é mesmo? Mas calma, nem tudo são flores. É preciso estar bastante atento ao fato de que os
     elementos processados pelo forEach são determinados antes da primeira invocação da função de callback . Isso
     significa que os elementosque forem adicionados depois da chamada do método não serão vistos. O mesmo vale se
     os valores dos elementos existentes do Array forem alterados. O valor passado para o callback será o valor no
     momento em que o forEach visitá-lo. Avalie o código a seguir para entender o que isso quer dizer: */

      var canais = ["Globo", "Sbt", "Record"];
      canais.forEach(function (canal) {
        canais.push("RedeTV"); // este item será ignorado
        console.log(canal);
      })
    /*
        Veja com atenção o que foi feito. Atribuímos a uma variável chamada canais uma lista que representa canais da
     televisão aberta brasileira. A seguir, invocamos o forEach e, dentro do callback , inserimos o canal RedeTV na
     nossa lista. Ao executar o código, podemos ver que a RedeTV nunca é exibida:
        +--------------------------------------------+
        |   Globo                                    |
        |   Sbt                                      |
        |   Record                                   |
        +--------------------------------------------+
     Isso acontece exatamente porque os elementos processados pelo forEach são determinados antes da primeira invocação
     da função de callback . Entretanto, isso não quer dizer que os valores não foram adicionados à lista. Ao adicionar
     um segundo console.log ao final do código para exibir a lista, notamos que a RedeTV foi adicionada várias vezes ao
     nosso Array . Uma cópia para cada iteração:
     */
     var canais1 = ["Globo", "Sbt", "Record"];
     canais1.forEach(function(canal) {
       canais1.push("RedeTV"); // este item será ignorado
       console.log(canal);
     })
     console.log(canais1);
     /*
      Resultado
        +-------------------------------------------------------------+
        |  [ 'Globo', 'Sbt', 'Record', 'RedeTV', 'RedeTV', 'RedeTV' ] |
        +-------------------------------------------------------------+
     */
  })
  it('ITERAÇÕES COM ARRAY - MAP', () => {
     /*
      O método map é muito útil quando precisamos não somente passar por todos os elementos de um Array , mas também
      modificá-los. Por exemplo, imagine que precisamos de um algoritmo para duplicar todos os valores de uma lista de
      números naturais. Sem pensar muito, faríamos algo assim:
     */
      var numeros = [1,2,3];
      var dobro = [];
      for(var i = 0; i < numeros.length; i++) {
        dobro.push(numeros[i] * 2);
      }
      console.log(numeros); // [1,2,3]
      console.log(dobro); // [2,4,6]
     /*
      Criamos um novo Array chamado dobro e usamos o seu método push para inserir o dobro de cada um dos valores
      recuperados por índice na iteração dos numeros . Podemos ter o mesmo comportamento ao usar o map :
      */
      var numeros1 = [1,2,3];
      var dobro1 = numeros1.map(function(numero) {
        return numero * 2;
      });
      console.log(numeros1); // [1,2,3]
      console.log(dobro1); // [2,4,6]
      /*
         O map executa a função de callback recebida por parâmetro para cada elemento iterado de numeros e constrói um
       novo Array com base nos retornos de cada uma das chamadas. Como o map nos devolve uma outra instância de Array ,
       a lista original nunca é realmente modificada, o que mantém sua integridade.
         E assim como no vimos no forEach , a função de callback não passa por elementos que foram modificados,
       alterados ou removidos depois da primeira execução da função de retorno.
       */
  })

  it('ITERAÇÕES COM ARRAY - FFILTER', () => {
    /*
     Como o próprio nome já pode induzir, este método é deve ser utilizado quando temos a necessidade de filtrar nossa
     lista de acordo com algum critério. Por exemplo, imagine que queremos filtrar de uma lista de alunos, todos os que
     são maiores de idade. Com o ES5, nós poderíamos fazer:
    */
    var alunos = [
      {nome:'joão', idade:15},
      {nome:'josé', idade:18},
      {nome:'maria', idade:20}
    ];
    var alunosDeMaior = [];
    for(var i = 0; i < alunos.length; i++) {
      if(alunos[i].idade >= 18) {
        alunosDeMaior.push(alunos[i]);
      }
    }
    console.log(alunosDeMaior);
   // [{nome:'josé', idade:18}, {nome:'maria', idade:20}]

    /*
     Com o método filter , temos o mesmo efeito de forma mais clara: */

    var alunos1 = [
      {nome:'joão', idade:15},
      {nome:'josé', idade:18},
      {nome:'maria', idade:20}
    ];
    var alunosDeMaior = alunos1.filter(function(aluno) {
      return aluno.idade >= 18;
    });
    console.log(alunosDeMaior);
   // [{nome:'josé', idade:18}, {nome:'maria', idade:20}]
   /*
    A função de callback recebe como parâmetro cada um dos alunos da lista em cada iteração — assim como aconteceu nas
    outras funções auxiliares que vimos — e o atribui na variável aluno. Dentro da função, utilizamos um critério de
    avaliação para devolver um valor booleano para o filter : true ou false . Se for retornado verdadeiro, o valor é
    inserido no novo Array retornado; caso contrário, é simplesmente ignorado e não é incluído. */
  });

  it('ITERAÇÕES COM ARRAY - FIND', () => {
    /*
     Esta função auxiliar é particularmente interessante quando o objetivo é encontrar um item específico dentro de um
     Array. Digamos, por exemplo, que de uma lista de alunos queremos  somente o registro que contenha o nome “josé”.
     O que faríamos tradicionalmente é algo nesse sentido:
    */
    var alunos = [
      {nome:'joão'},
      {nome:'josé'},
      {nome:'maria'}
    ];
    var aluno;
    for(var i = 0; i < alunos.length; i++) {
      if(alunos[i].nome === 'josé') {
        aluno = alunos[i];
        break; // evita percorrer o resto da lista
      }
    }
    console.log(aluno); // {"nome":"josé"}
    /*
     Para cada elemento da lista, recuperamos a propriedade do elemento e o comparamos com o nome que estamos buscando.
     Se der igualdade, atribuímos o valor na variável aluno instanciada antes do loop e o encerramos. Com o find ,
     é possível reescrever este código e obter o mesmo efeito, com a ressalva de que vamos pegar somente o primeiro
     item que satisfaz o critério de busca. Fica assim:
    */
    var alunos1 = [
      {nome:'joão'},
      {nome:'josé'},
      {nome:'maria'}
    ];
    var aluno1 = alunos1.find(function(aluno1) {
      return aluno1.nome === 'josé';
    });
    console.log(aluno1); // {"nome":"josé"}
    /*
     Caso na lista existissem dois alunos com o nome “josé”, somente o primeiro seria retornado. Para contornar este
     caso, precisaríamos usar um critério de busca mais específico.
    */
  });
  it('ITERAÇÕES COM ARRAY - EVERY', () => {
    /*
     Esta é uma função auxiliar bem interessante. Ao contrário das outras que vimos até então, esta não retorna uma
     cópia do Array , mas sim um valor booleano. A função every é pertinente para validar se todos os elementos de um
     Array respeitam uma dada condição. Para exemplificar, vamos novamente utilizar o cenário dos alunos maiores de
     idade. Mas para este caso, queremos saber se todos os alunos são maiores de idade. Primeiro, fazemos da forma convencional:
    */
    var alunos = [
      {nome:'joão', idade: 18},
      {nome:'maria', idade: 20},
      {nome:'pedro', idade: 24}
    ];
    var todosAlunosDeMaior = true;
    for(var i = 0; i< alunos.length; i++) {
      if(alunos[i].idade < 18) {
        todosAlunosDeMaior = false;
        break;
      }
    }
    console.log(todosAlunosDeMaior); // true
    /*
     Iteramos toda a lista procurando por alunos menores de idade. Ao achar um, já encerramos a iteração e retornamos
     false . Agora, observe como podemos simplificar essa lógica usando o every :
    */
    var alunos1 = [
      {nome:'joão', idade: 18},
      {nome:'maria', idade: 20},
      {nome:'pedro', idade: 24}
    ];
    var todosAlunosDeMaior = alunos1.every(function(aluno){
      return aluno.idade > 18;
    });
    console.log(todosAlunosDeMaior); // true
    /*
     A função itera cada um dos elementos sob a condição de aluno.idade > 18 e usa o operador lógico E (AND) em cada
     um dos retornos. Em outras palavras, caso um dos elementos não satisfaça a condição, o resultado do every de
     imediato será false . Caso todos atendam à condição, o true é retornado como resultado da função.
    */
  });
  it('ITERAÇÕES COM ARRAY - SOME', () => {
    /*
     Se a tarefa é validar se, pelo menos, um dos elementos de um Array satisfaz uma dada condição, o some é o método
     perfeito para o trabalho. Imagine que trabalhamos no setor de tecnologia de um aeroporto e precisamos desenvolver
     um pequeno programa para saber se alguma das malas de um passageiro está acima do limite máximo estabelecido de
     30kg. Usando um loop com for , o código para tal lógica é semelhante a este:
    */
    var pesoDasMalas = [12,32,21,29];
    var temMalaAcimaDoPeso = false;
    for(var i = 0; i < pesoDasMalas.length; i++) {
      if(pesoDasMalas[i] > 30) {
        temMalaAcimaDoPeso = true;
      }
    }
    console.log(temMalaAcimaDoPeso); // true

    /*
     Mesmo o código não tendo muita complexidade, podemos torná-lo ainda mais claro, objetivo e enxuto utilizando
     o some .*/
     var pesoDasMalas = [12,32,21,29];
     var temMalaAcimaDoPeso = pesoDasMalas.some(function(pesoDaMala) {
       return pesoDaMala > 30;
     });
    console.log(temMalaAcimaDoPeso); // true
    /*
     Para cada peso de mala contida no pesoDasMalas , é verificado se ele é superior a 30 kg. Na primeira ocorrência
     de caso positivo para a condição, a execução do loop é interrompida e o método retorna true . Caso contrário, o
     Array todo é percorrido e o método retorna false se chegar ao final sem encontrar um registro que satisfaça a
     condição.
     */

  });
  it('ITERAÇÕES COM ARRAY - REDUCE', () => {
    /*
     A função auxiliar reduce foi deixada para o final por ser a mais complicada. A ideia por trás dela é pegar todos
     os valores de um Array e condensá-los em um único. Para demonstrar seu funcionamento, vamos mostrar um caso
     clássico de uso.  Neste exemplo, vamos fazer a soma de todos os elementos de dentro de um Array . Como fizemos
     nos outros, primeiro implementamos uma abordagem mais comum:
    */
    var numeros = [1,2,3,4,5];
    var soma = 0;
    for (var i = 0; i < numeros.length; i++) {
      soma += numeros[i];
    }
    console.log(soma); // 15

    /*
     Aqui não tem segredo. Apenas iteramos a lista com um laço de
     repetição e usamos a variável soma , inicializada em 0 , para
     acumular o resultado. Agora perceba como temos o efeito equivalente usando a função reduce:
    */
    var numeros1  = [1,2,3,4,5];
    var soma1 = 0;
    soma1 = numeros1.reduce(function(soma1, numero){
      return soma1 + numero;
    },0)
    console.log(soma1); // 15

    /*
       Diferentemente dos outros métodos vistos até agora, o reduce aceita dois parâmetros:
         * function(soma1,numero){...}: função de iteração oom os dois parâmetros;
         * 0: Valor inicial
       Para cada iteração, o valor da soma se torna o valor retornado da iteraçã anterior, sendo que na promeira
       chamada o valor inicial é o que definimos como o segundo parâmetro da função, neste caso o número zero.

       Para fixar melhor, veremos um segundo exemplo. Agora temos uma lista de alunos que possuem duas características:
       nome e idade . Imagine que queremos uma lista com somente os nomes dos alunos, ignorando a idade. Podemos
       utilizar o reduce para nos ajudar da seguinte maneira:
    */
    var alunos = [
      {nome:'joão', idade: 10},
      {nome:'josé', idade: 20},
      {nome:'marcos', idade: 30}
    ];
    var nomes = alunos.reduce(function(arrayNomes, aluno) {
      arrayNomes.push(aluno.nome);
      return arrayNomes;
    }, []);
    console.log(nomes); // ['joão', 'josé', 'marcos']

    /*
     Vamos avaliar o funcionamento deste código. Na lista alunos , chamamos o método reduce , e nele passamos a função
     de iteração anônima com dois parâmetros, arrayNomes e aluno ; e um Array vazio ( [ ] ) como valor inicial. Em cada
     iteração, colocamos o nome do aluno no Array de nomes e o retornamos, ou seja, esta variável itera toda a lista e
     recupera os valores que interessam. De certo modo, condensou o Array em um único valor.
    */
  });

  it('ITERAÇÃO COM ITERADORES E ITERÁVEIS', () => {
    /*
     A iteração é definida por dois conceitos centrais: iteradores e iteráveis. Um iterável está ligado com um iterador
     que define como ele será percorrido. O seu objetivo é prover uma forma de sequencialmente acessar os elementos de
     um iterável sem expor sua representação interna, retirando a responsabilidade dele de saber como acessar e caminhar
     sobre sua estrutura. Vamos entender exatamente o que isso quer dizer.
        * ITERADORES
           Definimos como um iterador um objeto que sabe como acessar, um a um, os itens de um iterável, enquanto mantém
           o status da sua posição atual na estrutura. Esses objetos oferecem o método next, que retorna o próximo item
           da estrutura do iterável sempre que invocado.

           Na realidade, este método retorna um outro objeto com duas propriedades: done e value . O done é um valor
           booleano que indica se toda a estrutura foi acessada, enquanto o value contém o valor extraído.

           Por exemplo, se tivermos uma coleção com um único número (o número 1) e chamarmos o método next uma vez,
           obteremos este valor:

              iteravel.next(); // {value: 1, done: false}

           Se chamamos novamente o next , não temos mais valor, pois a coleção inteira já foi percorrida. Entretanto,
           temos a indicação de que ela foi finalizada na propriedade done que retornará true :

              iteravel.next(); // {value: undefined, done: true}

        * ITERÁVEIS
          Um objeto é definido como iterável se ele define explicitamente o seu comportamento de iteração. Para isso,
          é necessário que ele implemente o seu iterador na propriedade de chave Symbol.iterator (veremos o porquê deste
          nome com mais detalhes no capitulo Funções geradoras). No JavaScript, alguns tipos já são iteráveis por
          padrão:
             * Arrays;
             * Strings;
             * Maps;
             * Sets;
          Para estes tipos, podemos obter o seu iterador e usá-lo. Por exemplo, veja o  objeto bruxos do tipo Array do
          exemplo a seguir: */

              var bruxos = ['harry Potter', 'Hermione Granger', 'Rony Weasley'];
              // obtem o iterador
              var iteradorBruxos = bruxos[Symbol.iterator]();
              console.log(iteradorBruxos.next()); //{value: Harry Potter, done: false}
              console.log(iteradorBruxos.next()); //{value: Hermione Granger, done: false}
              console.log(iteradorBruxos.next()); //{value: Rony Weasley, done: false}
              console.log(iteradorBruxos.next()); //{value: undefined, done: true}
          /*
          Recuperamos o iterador da propriedade Symbol.iterator e usamos o seu método  next para passar por toda a lista
          */


  });


  it('ITERAÇÕES COM GERADORES', () => {
    function *percorrerLinha477() {
      console.log('Passei pela rua 1')
      yield 'Parada 1'
      console.log('Passei pela rua 2')
      yield 'Parada 2'
      console.log('Passei pela rua 3')
      yield 'Parada 3'
      console.log('Passei pela rua 4')
      yield 'Fim da linha'
    }

    const linha = percorrerLinha477()
    for (let parada of linha) {
      console.log(parada)
    }
  })
  it('ITERAÇÕES COM GERADORES', () => {
    /*
       No caso das funções geradoras, elas também possuem um
     método de iteração na propriedade Symbol.iterator definido. É
     por isso que, quando as colocamos no laço do tipo for...of , ele é
     iterado perfeitamente. O mesmo pode ser feito para qualquer
     estrutura de dado que você definir. Por exemplo, vamos supor que
     temos uma estrutura que representa uma equipe de
     desenvolvimento da analitica sistemas.
     ---------------------------------------------------------------
     const equipe = {
             quantidade: 5,
             maturidade: 'alta',
             administrativo: 'Marco',
             tecnologia: 'Andre',
             comercial: 'Paulo Marcio',
             suporte1: 'Carlos',
             suporte2: 'Wilson',
     }
     ---------------------------------------------------------------
       Como podemos fazer para iterar os integrantes desta equipe,
     sendo que as propriedades quantidade e maturidade não
     interessam? Podemos definir uma função geradora no
     Symbol.Iterator para a nossa estrutura, de modo que ela retorna
     somente os membros da equipe:*/
    const equipe = {
      quantidade: 5,
      maturidade: 'alta',
      administrativo: 'Marco',
      tecnologia: 'Andre',
      comercial: 'Paulo Marcio',
      suporte1: 'Carlos',
      suporte2: 'Wilson',
      [Symbol.iterator]: function *() {
        yield this.administrativo
        yield this.tecnologia
        yield this.comercial
        yield this.suporte1
        yield this.suporte2
      }
    }
    for(let integrante of equipe) {
      console.log(integrante)
    }
  })
  it('DELEGAÇÃO DE FUNÇÕES GERADORAS', () => {
    /*
        Podemos combinar duas funções geradoras através da delegação de geradores.
     Imagine, por exemplo, que temos uma equipe de projetos que é subdividida em
     dois times: o time de desenvolvimento e o time de negócios. Vamos implementar
     este cenário.
        Primeiro, criamos um objeto literal para cada time, sendo que cada um deve
     conter: a quantidade de envolvidos, seus respectivos  cargos e uma função
     geradora para que possamos iterar estes times.*/
     // ------------------------------------------------------------------------------
     const timeDesenvolvimento = {
       quantidade: 5,
       maturidade: 'alta',
       administrativo: 'Marco',
       tecnologia: 'Andre',
       comercial: 'Paulo Marcio',
       suporte1: 'Carlos',
       suporte2: 'Wilson',
       [Symbol.iterator]: function *() {
           yield this.administrativo
           yield this.tecnologia
           yield this.comercial
           yield this.suporte1
           yield this.suporte2
       }
     }
     const timeNegocios = {
       quantidade: 2,
       financeiro: 'Jussara',
       secretaria: 'Daniele',
       [Symbol.iterator]: function *() {
          yield this.financeiro
          yield this.secretaria
       }
     }
     // ------------------------------------------------------------------------------
    /*  Agora que temos nossos times, precisamos definir que nossa
        equipe de projetos é constituída das duas.
          const equipe = {
            timeDesenvolvimento,
            timeNegocios
           };
       Para que seja possível criar uma função geradora que itere nossa
       equipe de projetos, assim como fizemos para os times de
       desenvolvimento e negócios, utilizamos a delegação de geradores.
       Usamos a palavra reservada yield acompanhada de um * (asterisco). Ele funciona como um link, uma porta, entre as duas
       funções geradoras.
     */
      const equipe = {
        timeDesenvolvimento,
        timeNegocios,
        [Symbol.iterator]: function *() {
          yield *timeDesenvolvimento
          yield *timeNegocios
        }
      }
      /*
           Agora ao iterar a nossa equipe com o for...of , temos o nome
        de todos os integrantes da nossa equipe.
        for(let integrante of equipe) {
        console.log(integrante);
       } */
    for(let integrante of equipe) {
      console.log(integrante)
    }
  })
  it('OPERAÇÕES ASSÍNCRONAS COM PROMISES - exemplo 1', () => {
    /*
       Promises são uma alternativa criada para lidar com resultados de operações
     assíncronas. Como a execução do JavaScript é ininterrupta (mesmo com o uso de
     funções como setTimeout ou setInterval ), temos um problema quando um certo
     trecho do nosso código depende do resultado de uma operação que não sabemos
     quanto tempo demorará até ser completada.
       Uma situação típica na qual temos este problema é quando consumimos APIs REST
     em nossas aplicações. As solicitações são feitas para um webservice e não sabemos
     de antemão quanto tempo este serviço vai demorar para enviar uma resposta. Ao mesmo
     tempo, o trecho de código seguinte à requisição necessita do resultado desta
     operação; caso contrário, não tem como executar sua lógica. Resultado: podemos ter
     o valor indefinido no momento que o código é executado.
       A abordagem mais convencional para lidar com operações deste tipo são os callbacks.
       Para uma operação em que o tempo de execução é indefinido, usamos uma função de
     retorno que será chamada somente quando a operação for finalizada. Como no trecho a
     seguir, onde passamos dois argumentos para a final de sua execução:
       +---------------------------------------------------------------------+
       |  funcaoAssincrona(arg1, callback){                                  |
       |    // faz request e afins                                           |
       |    // e no final da execução executamos o callback                  |
       |    callback();                                                      |
       |  }                                                                  |
       |  function callback() {                                              |
       |    / operação que quero fazer depois que tiver a resposta da request|
       |  }                                                                  |
       +---------------------------------------------------------------------+
     O problema desta abordagem é que, uma vez que você começa a trabalhar com múltiplas
     operações assíncronas, fica complicado entender a sequência de operações que está
     sendo executada, pois cada trecho vai depender do outro, que depende do outro, assim
     sucessivamente. No final, você acabará com um código assim:
        +---------------------------------------------------------------------+
        |  obj.funcaoAssincrona(function(response) {                          |
        |     response.funcaoAssincrona(function(response2) {                 |
        |        response2.funcaoAssincrona(function(response3) {             |
        |           response3.funcaoAssincrona(function(response4) {          |
        |              return response4;                                      |
        |           });                                                       |
        |        });                                                          |
        |     });                                                             |
        |  });                                                                |
        +---------------------------------------------------------------------+
        Para não ter que lidar mais com este problema, srgiram as implementações
     das promises, que nada mais são do que objetos que ontêm refêrencias para
     funções que são executads quando as operações assincoronas são terminadas
     com êxito ou falhas. Vamos ver como elas funcinam.
        Uma promise, em sua essência, possui três estados:
          * NÃO RESOLVIDO: estado inicial, quando está esperando algo ser finalizxado
          * RESOLVIDO: estado no qual a operação foi realizada sem erros.
          * REJEITADO: estado no qual a operação foi concluída porém com erros.
        Para os dois últimos esados, resolvido e rejeitado, associamos funções que
     queremos que sejam executadas. Para isso, usamos as palavras reservadas then
     e catch. Sabendo disso, já podemos começar a codificar e construir o esqueleto de
     uma promise.
        +---------------------------------------------------------------------+
        |  let promise = new Promisse((resolve, reject) => {                  |
        |    // corpo da promise                                              |
        |  });                                                                |
        |  promise.then();                                                    |
        |  promise.catch();                                                   |
        +---------------------------------------------------------------------+
     Por padrão, o construtor da promise recebe uma função com dois argumentos: resolve
     e reject . Utilizamos estes parâmetros dentro da lógica de nossa promise para indicar
     quando ela foi resolvida ou rejeitada. Quando um promise é resolvida sem problemas, o
     then é automaticamente ativado, assim como acontece com o catch quando a promise é
     rejeitada. Vamos ver um exemplo mais prático que simula esta situação com o código a
     seguir:
     */
     let promise = new Promise((resolve, reject) => {
       let resultado = true
       if(resultado) {
         resolve('deu tudo certo!')
       } else {
         reject('deu tudo errado!')
       }
     })
    /*
     Com este código, criamos uma situação simulada do uso de uma promise. A variável resultado
     contém o que seria o resultado da operação assíncrona. Se tudo deu certo, o resolve é
     invocado com a String "deu tudo certo". No caso de falha, utilizamos o reject com a String
     "deu tudo errado". Para recuperar estes dados que estão sendo passados, usamos o parâmetro
     data que por padrão também é definido dentro das funções then e reject .
    */
    promise.then( data => console.log(`resultado positivo: ${data}`))
    promise.catch(data => console.log(`resultado negativo: ${data}`))
  })

  it('OPERAÇÕES ASSÍNCRONAS COM PROMISES - exemplo 2', () => {
    /*
        Para facilmente simular a execução de uma função assíncrona dentro da nossa promise — como
     uma chamada AJAX, por exemplo — vamos utilizar a função setTimeout .
    */
    let promise = new Promise((resolve, reject) => {
      let resultado = false
      let tempo = 1 // milisegundos
      setTimeout(() => {
        if (resultado) {
          resolve('deu tudo certo!')
        } else {
          reject('deu tudo errado!')
        }
      }, tempo)
    })
    promise.then(data => console.log(`resultado positivo: ${data}`))
    promise.catch(data => console.log(`resultado negativo: ${data}`))
    /*
     Colocamos a execução do nosso código dentro do setTimeout e definimos o tempo em uma variável
     com o mesmo nome, com o valor de 2000, que representam dois segundos. Para tornar o cenário
     ainda mais semelhante ao do mundo real, vamos executar algum código fora da promise. Vamos colocar
     a seguinte linha no final do código: */

       console.log('fui executado antes!')
  })
  it('OPERAÇÕES ASSÍNCRONAS COM PROMISES - ANINHAMENTO DE THEN E CATCH', () => {
    let promise = new Promise((resolve, reject) => {
      let resultado = true
      if (resultado) {
          resolve('deu tudo certo!')
        } else {
          reject('deu tudo errado!')
      }
    })
    /*
     As funções then e catch possuem várias características importantes que precisamos saber antes de
     sair usando. A primeira delas é que podemos aninhar suas chamadas. Por exemplo, na declaração de
     uma promise, em vez de declarar os métodos then e catch de modo separado, como fizemos antes, podemos
     fazer de modo aninhado:
    */
    promise
      .then(data => console.log(`resultado positivo: ${data}`))
      .catch(data => console.log(`resultado negativo: ${data}`))
    /*
     Do mesmo modo, podemos aninhar várias chamadas do then .
     Isso faz com que cada um seja executado após o outro:
     */
    promise
      .then(data => console.log(`resultado positivo: ${data}`))
      .then(data => console.log(`resultado positivo 2: ${data}`))
      .catch(data => console.log(`resultado negativo: ${data}`))

    /*
     Entretanto, é preciso estar atento quando usamos aninhamento  de then . Repare que, ao executar o código
     do exemplo anterior, com a promise resolvida, notaremos algo estranho na saída do console:
        +--------------------------------------------+
        |  resultado positivo: deu tudo certo!       |
        |  resultado positivo 2: undefined           |
        +--------------------------------------------+
     Isso acontece porque o valor da variável data , disponível na primeira chamada do then , não é passada adiante.
     Ou seja, o valor da variável data é sempre correspondente ao retorno da função anterior. Isso significa que, para
     que tenhamos a mensagem “deu tudo certo!” nas duas execuções do then , precisamos alterar nosso código.
     Retornaremos a variável data dentro da execução do then :

     */

    promise
      .then(data => {
        console.log(`resultado positivo **1: ${data}`)
        return data
      })
      .then(data => console.log(`resultado positivo **2: ${data}`))
      .catch(data => console.log(`resultado negativo: ${data}`))
    /*
     Agora sim, ao executar o código, temos as duas mensagens sendo exibidas, como esperado:
       +--------------------------------------------+
       |  resultado positivo: deu tudo certo!       |
       |  resultado positivo 2: deu tudo certo!     |
       +--------------------------------------------+
    */
  })
  it('OPERAÇÕES ASSÍNCRONAS COM PROMISES - ERROS INESPERADOS', () => {
    /*
     Quando utilizamos o reject nas nossas promises, estamos tratando os casos em que já esperamos
     de antemão que algum cenário possa dar problema. Entretanto, há situações nas quais erros que
     o desenvolvedor não esperava podem acontecem. No exemplo a seguir, vamos inserir um erro
     intencionalmente na execução do código para simular este cenário.
    */
    let promise = new Promise((resolve, reject) => {
      throw new Error('erro!')
      resolve('ok!')
    })
    promise
      .then(data => console.log(`sucesso: ${data}`))
      .catch(data => console.log(`falha: ${data}`))
    /*
     Mesmo esperando que o resolve ative a função then , como um erro está sendo lançado, o catch é ativado.
       +--------------------------------------------+
       |  falha: Error: erro!                       |
       +--------------------------------------------+

     O mesmo acontece quando temos vários then aninhados. Se um deles dá algum erro, o catch é automaticamente
     ativado. No exemplo seguinte, jogamos um erro propositalmente na primeira execução do then :
     */
      let promise1 = new Promise((resolve, reject) => {
        resolve('ok!')
      })
      promise1
        .then(data => {
          console.log(`sucesso: ${data}`)
          throw new Error('erro!')
          return data
        })
        .then(data => console.log(`sucesso: ${data}`))
        .catch(data => console.log(`falha: ${data}`))

      /*
       Ao rodar este código, vemos que o segundo log de sucesso nunca é exibido.
         +--------------------------------------------+
         |  sucesso: ok!                              |
         |  falha: Error: erro!                       |
         +--------------------------------------------+


       */
  })
  })

